var utils = (function(){

 $(function (){

  //sidenav init
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
  });

  //alert('init')

 	if($('footer').length){
   stickyFooter();
	}

	if($('select').length){
   $('select').formSelect();}

  M.updateTextFields();
  
  if($('#textarea1').length){
    M.textareaAutoResize($('#textarea1'));}
  
  if($('#textarea2').length){
    M.textareaAutoResize($('#textarea2'));}
  
   $('.sidenav').sidenav();
  
   tosOnSubmit();

   $('.button-collapse').sideNav({
      closeOnClick: true
    }
  );
  
  });

 //footer
 var stickyFooter = function(){
   
   var docHeight = $(window).height();
   var footerHeight = $('footer').height();
   var footerTop = $('footer').position().top + footerHeight;

   if (footerTop < docHeight) {
    $('footer').css('margin-top', 0+ (docHeight - footerTop) + 'px');
   }
}

//validate tos and gdpr
var tosIsChecked = function(){
  var retval = true;
 $('.tosgdpr').each(function(){
  if(this.checked){
    retval = true;
    return false;
  }
  else{
    retval = false;
  }
 });
 return retval;
}

var tosIs2Checks = function(){
  var retval;
   if($('#tosGdprAgreeYes').is(':checked') && $('#tosGdprAgreeNo').is(':checked')){
     
      retval = true;
   }
   else
   {
    retval = false;
   }
   return retval;
}

//validate on submitin add_order
var tosOnSubmit = function(){
 // alert(tosIs2Checks())
  $("#submitSite").on('click',function(e){
    if(tosIs2Checks()){
         e.preventDefault();
        alert('Please check only one choice, in Terms & GDPR selection')
        return false; 
    }


    if(!tosIsChecked())
      {  
        e.preventDefault();
        alert('Please check Terms and GDPR agreement.');
        return false; 
      }
  });
}

return {
 stickyFooter: stickyFooter,
 tosOnSubmit: tosOnSubmit
};
})();
 
