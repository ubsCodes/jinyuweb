# for building the files and functions ofa site
# make files, get variables from the input in add_order, 
# append the variables to the files
 
from bottle import route, template, request, static_file, error
import os, sys
import zipfile
import urllib2


def build_files():
    # create an html5 file
    f = open("buildbots/dist/ursite.html","w+")
    f.write("<!--sample html5--->")
    f.close()

    # create preview tpl
    i = open('buildbots/yoursite.tpl','w+')
    i.write('<!--preview site tpl-->')
    i.close()    

    #create css
    h = open("buildbots/dist/urcss.css","w+")
    h.write('/* ur css theme */')
    h.close()

    #create js
    e = open('buildbots/dist/urjs.js','w+')
    e.write('console.log(\'sample js\')')
    e.close()

def apply_sticky_footer():
    sticky_footer = """ 
    
    var utils = (function(){

     $(function (){
        stickyFooter();
         });

          //footer
           var stickyFooter = function(){

              var docHeight = $(window).height();
                 var footerHeight = $('footer').height();
                    var footerTop = $('footer').position().top + footerHeight;

                       if (footerTop < docHeight) {
                                   $('footer').css('margin-top', 0+ (docHeight - footerTop) + 'px');
                                      }

                        }

     
                        return {

                         stickyFooter: stickyFooter,

                         };


                         })();

    
    """
    j = open('buildbots/dist/urjs.js','w')
    j.write(sticky_footer)
    j.close()

def apply_theme(csstheme):
    # print
    # switch 
    the_css = ""
    if csstheme == '90s':
        the_css = """ .container { margin-left: 20%; margin-right: 20%; margin-top: 5%; width: 40%; 
                      font-family: Arial, Helvetica, sans-serif} 
            .nav {margin-left: 20%; margin-right: 20%; padding-left: 5px; border: 1px solid darkgray; 
                  border-radius: 2px; height: 25px;
                  background: linear-gradient(to bottom,  #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%);}
            .foot {margin-left: 20%; margin-right: 20%; border-top: 1px solid black}
            .aside { position: relative; top: -600px; left: 800px}
            .aside img { height: 400px; width: 200px; }
             h1, h2 { margin-left: 20%; font-size: 1em; margin-top: 2%}
             .h2 { font-weight: bold }
             .cta { margin-left: 25%; margin-top: 25px; width: 13%; padding: 5px; border-radius: 2px; background: red; font-size: 2em; color: white}
            """
    elif csstheme == 'simple':
        the_css = """ .container { width: 100%; margin: 5%; font-family: Helvetica, Arial, 'Nimbus Sans L', sans-serif }
            .nav { padding: 10px; font-size: 1em; background: skyblue; width: 100%; height: 25px}
            .h1, .h2, .aside, .article { margin-top: 50px;}
            .foot { padding: 2%; width: 100%; height: 180px; background: white; postition:relative; top: 50%}
            body { margin: 0 }
            .cta { padding: 8px; margin-top: 2%; margin-left: 9%; width: 14%; border-radius: 2px; background: red; font-size: 2em; color: white}
            .aside { height: 500px; width: 300px; display: block; float: right}
            .aside img { height: 500px; width: 300px; display: block; float: right}
            .row { width: 100% }
            .col-md-4 { width: 25%; display: inline-block }
            h2, h1 { color: royalblue; font-size: 2em; margin-left: 5%;}
            p { color: gray;}
            """
    elif csstheme == 'hipncool':
        the_css = """ 
            @import url('https://fonts.googleapis.com/css?family=Lobster');
            .container { margin: 5px; font-family: Helvetica, Arial, 'Nimbus Sans L', sans-serif; }
            .nav { color: darkred; padding: 10px; font-size: 1em; background: pink; width: 100%; height: 30px; }
            .h1, .h2, .aside, .article { margin-top: 2%; margin-left: 5%; font-family: 'Lobster', cursive; font-size; 14px}
            .foot { padding: 2%; background-color: pink; width: 100%; height: 200px; font-size: 14px; postition:relative; top: 50%}
            body { margin: 0 } 
            .cta { margin-top: 2%; margin-left: 9%; width: 13%; padding: 8px; border-radius: 2px; background: royalblue; font-size: 2em; color: white}
            h2, h1 { text-shadow: -1px 0 darkred, 0 1px darkred, 1px 0 darkred, 0 -1px darkred; color: pink; 
                    font-size: 2em !important; margin-left: 5%; font-family: 'Lobster', cursive}
            .aside { height: 500px; width: 300px; margin: 5%; }
            .aside img { height: 500px; width: 300px; position: relative; top: 30px;}
            .row { width: 100% }
            .col-md-4 { width: 25%; display: inline-block }
            p { color: darkred;}
            """


    else:
        the_css = """ .container { width: 100%; margin: 5%; font-family: Helvetica, Arial, 'Nimbus Sans L', sans-serif }
            .nav { padding: 10px; font-size: 1em; background: skyblue; width: 100%; height: 25px}
            .h1, .h2, .aside, .article { margin-top: 50px;}
            .foot { padding: 2%; background: white; width: 100%; height: 180px}
            body { margin: 0 }
            .cta { padding: 8px; margin-top: 2%; margin-left: 9%; width: 14%; border-radius: 2px; background: red; font-size: 2em; color: white}
            .aside { height: 500px; width: 300px; display: block; float: right}
            .aside img { height: 500px; width: 300px; display: block; float: right}
            .row { width: 100% }
            .col-md-4 { width: 25%; display: inline-block }
            h2, h1 { color: royalblue; font-size: 2em; margin-left: 5%;}
            p { color: gray;}
            """
    # append css file
    g = open('buildbots/dist/urcss.css','w')
    g.write(the_css)
    g.close()

def build_site2(sitetitle, sitemenu, sitename, sitetag, sitetheme):
    

    if sitetheme == 'bootstrap':

        bootstrap = """ 
  <!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content=""
     
          
     <title>{title} - {site_tag}</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet">

     </head>

     <body>

     

     <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">{site_name}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">{site_menu} <span class="sr-only">(current)</span></a>
          </li>
        <ul>
      </div>
      </nav>    
     
     
     <div class="container">
      <div class="jumbotron">
            <h1>{site_name}</h1>
            <p>{site_tag}</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
        </div>
      </div>
     
     
     
     <main role="main">

     <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
        </div>

        <hr>

      </div> <!-- /container -->

    </main>

     <footer class="container">
      <p>copyright &copy; {site_name}.com &bull; all rights reserved</p>
    </footer>

    

     <script src='https://code.jquery.com/jquery-3.3.1.min.js'
                  integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8='
                  crossorigin='anonymous'></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
     <script src='urjs.js'></script>  
     
     </body>

     </html> """.format(title=sitetitle, site_menu=sitemenu, site_name=sitename, site_tag=sitetag)

        create_html(bootstrap)
        create_tpl(bootstrap)
    # google materialize

    elif sitetheme == 'materialize':

        materialize = """
        
         <!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>{title} - {site_tag}</title>


    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
</head>

<body>

    <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">{site_name}</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">{site_menu}</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>            


    <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center orange-text">{site_name}</h1>
      <div class="row center">
        <h5 class="header col s12 light">{site_tag}</h5>
      </div>
      <div class="row center">
        <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light orange">Get Started</a>
      </div>
      <br><br>

    </div>
  </div>

  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Speeds up development</h5>

            <p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
            <h5 class="center">Easy to work with</h5>

            <p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
          </div>
        </div>
      </div>

    </div>
    <br><br>
  </div>

  <footer class="page-footer orange"> <p>copyright &copy; {site_name}.com &bull; all rights reserved</p></footer>
       

   <script src='https://code.jquery.com/jquery-3.3.1.min.js'
                  integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8='
                  crossorigin='anonymous'></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
     <script src='urjs.js'></script>  
     
     </body>

     </html> """.format(title=sitetitle, site_menu=sitemenu, site_name=sitename, site_tag=sitetag)     

        create_html(materialize)
        create_tpl(materialize)
  
    elif sitetheme == 'windowsflat':
        flatwindows = """
            <!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Metro 4 -->
    <link rel="stylesheet" href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css">
  </head>
  <body>
  
  <div class='container'>
    <ul class="h-menu bg-red fg-white">
    <li><a href="#">{site_name}</a></li>
    <li><a href="#">{site_menu}</a></li>
    </ul>

    <div class="grid">
    <div class="row">
        <div class="cell-6">
            <h2 class="fg-cyan">{site_tag}</h2>
        <hr class="bg-cyan">

        </div>
        <div class="cell-6"></div>
    </div>
    <div class="row">
        <div class="cell-12">
            <div>
            Improve access management experiences for end users and administrators,
            including self service password reset, group management,
            sign in customization, and reporting.
            <br><br>
            <button class="button info">Start Now!</button>
        </div>

        </div>
    </div>
    </div>

    <button class="command-button" data-role="ripple" data-ripple-color="#1ba1e2">
    <span class="mif-share icon"></span>
    <span class="caption">
        Yes, share and connect
        <small>Use this option for home or work</small>
    </span>
</button>
   


   <div class="grid">
    <div class="row">
        <div class="cell-4">
            <div class="card image-header">
    <div class="card-header fg-white"
         style="background-image: url(https://dummyimage.com/300x500/000000/fff)">
        Journey To Mountains
    </div>
    <div class="card-content p-2">
        <p class="fg-gray">Posted on January 21, 2015</p>
        Quisque eget vestibulum nulla. Quisque quis dui quis ex
        ultricies efficitur vitae non felis. Phasellus quis nibh
        hendrerit...
    </div>
    <div class="card-footer">
        <button class="button secondary">Read More</button>
    </div>
</div>
        </div>
        <div class="cell-4">
            <div class="card">
    <div class="card-header">
        Card header
    </div>
    <div class="card-content p-2">
        Card with header and footer...
    </div>
    <div class="card-footer">
        Card Footer
    </div>
</div>
        </div>
        <div class="cell-4">

                <div class="card">
    <div class="card-header">
        <div class="avatar">
            <img src="https://dummyimage.com/600x400/000/fff&text=img">
        </div>
        <div class="name">John Doe</div>
        <div class="date">Monday at 3:47 PM</div>
    </div>
    <div class="card-content p-2">
        <img src="https://dummyimage.com/600x400/000/fff&text=img" style="width: 100%">
    </div>
    <div class="card-footer">
        <button class="flat-button mif-thumbs-up"></button>
        <button class="flat-button mif-tag"></button>
        <button class="flat-button mif-share"></button>
    </div>
</div>
        </div>
    </div>
    </div>


<footer> <p>copyright &copy; {site_name}.com &bull; all rights reserved</p></footer>
  
</div>

    <!-- jQuery first, then Metro UI JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.metroui.org.ua/v4/js/metro.min.js"></script>
  </body>
</html>

        """.format(title=sitetitle, site_menu=sitemenu, site_name=sitename, site_tag=sitetag)

        
        create_html(flatwindows)
        create_tpl(flatwindows)

    else:
        print "unknown theme selected"    

def build_site(sitetitle, sitemenu, sitename, sitetag):
    # create template and add data
    # copy a css file
    # def (build_site(data) originally
    html5 = """ <!DOCTYPE html>
     <html>
     <head>
     <link rel='stylesheet' href='urcss.css'>
     <meta charset="UTF-8">
     <title>{title}</title>
     </head>

     <body>
     <div class='nav'>{site_name} &nbsp; {site_menu}</div>
     <h1>{site_name}</h1>
     <h2>{site_tag}</h2>
     <div class='cta'>Start now!</div>

     <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <div class="h2">Heading</div>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <div class="h2">Heading</div>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <div class="h2">Heading</div>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
        </div>
      </div> <!-- /container -->

     <div class='aside'>
     <div class='h2'>Ad Banners</div>
     <img src="https://dummyimage.com/300x500/000000/fff">
     </div>

     <footer class='foot'>copyright &copy; {site_name}.com &bull; all rights reserved</footer>
     
     
     <script src='https://code.jquery.com/jquery-3.3.1.min.js'
                  integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8='
                  crossorigin='anonymous'></script>

     <script src='urjs.js'></script>  
     </body>

     </html> """.format(title=sitetitle, site_menu=sitemenu, site_name=sitename, site_tag=sitetag)


    _tpl = """ <!DOCTYPE html>
     <html>
     <head>
     <link rel='stylesheet' href='/buildbots/dist/urcss.css'>
     <meta charset="UTF-8">
     <title>{title}</title>
     </head>

     <body>
     <div class='nav'>{site_name} &nbsp; {site_menu}</div>
     <div class='h1'>{site_name}</div>
     <div class='h2'>{site_tag}</div>
     <div class='cta'>Start now!</div>
          
      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
        </div>
      </div> <!-- /container -->

     <div class='aside'>
     <h2>Ad Banners</h2>
     <img src="https://dummyimage.com/300x500/000000/fff">
     </div>


     <footer class='foot'>copyright &copy; {site_name}.com &bull; all rights reserved</footer>
     
     
     <script src='https://code.jquery.com/jquery-3.3.1.min.js'
                  integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8='
                  crossorigin='anonymous'></script>

     <script src='/buildbots/dist/urjs.js'></script>  
     </body>

     </html> """.format(title=sitetitle, site_menu=sitemenu, site_name=sitename, site_tag=sitetag)

        
    
    create_html(html5)
    
    # build preview site
    create_tpl(_tpl)


def create_html(theme):
    f = open("buildbots/dist/ursite.html","w")
    f.write(theme)
    f.close()

def create_tpl(thetpl):
    # build preview site
    t = open('buildbots/yoursite.tpl','w')
    t.write(thetpl)
    t.close()

def builder_2(sitetitle, sitemenu, sitename, sitetag, sitetheme):
    build_files()
    build_site2(sitetitle, sitemenu, sitename, sitetag, sitetheme)   

def builder(sitetitle, sitemenu, sitename, sitetag, sitetheme):
    build_files()
    build_site(sitetitle, sitemenu,sitename,sitetag)
    apply_theme(sitetheme)
    apply_sticky_footer()

@route("/results/<thesite>/<ziptype>")
def build_results(thesite, ziptype):
    dirlist = os.listdir("./buildbots/dist")
    
    return template('build_results', dlist=dirlist, site=thesite, zipfile_type=ziptype)

# zip the created web files
def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            if file != '__init__.py':
                ziph.write(os.path.join(root, file))

# download zip web files
@route("/site/<filename:path>")
def dl_web(filename):
    return static_file(filename, root='./' ,download=filename)

# preview site
@route('/preview/<filename:path>')
def preview_web(filename):
    return static_file(filename, root='./buildbots/dist/')

