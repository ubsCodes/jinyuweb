% include('navi.tpl')

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Privacy Policy</h1>
      <div class="row center">
        <h5 class="header col s12 light">What we do about your personal info and also abiding with the GDPR laws</h5>
      </div>
    </div>
</div>

<div class="container z-depth-2">
  <div class="row center">
    <div class="col s12">
      <h2 class="flow-text center-align">Privacy Notice</h2>

      <p class="flow-text left-align light">This privacy notice discloses the privacy practices for www.jinyuweb.com. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p>

      <ol class="left-align">
      	<li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li>
      	<li>What choices are available to you regarding the use of your data.</li>
      	<li>The security procedures in place to protect the misuse of your information.</li>
      	<li>How you can correct any inaccuracies in the information.</li>
      </ol>

       <h2 class="flow-text center-align">Information Collection, Use, and Sharing</h2>

       <p class="flow-text left-align light">We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.

    		We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization .We may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>

        <h2 class="flow-text center-align">Your Access to and Control Over Information</h2>

         <p class="flow-text left-align light">
           You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or facebook account given on our website:
         </p>

         <ul class="browser-default left-align">
           <li>See what data we have about you, if any.</li>
           <li>Change/correct any data we have about you.</li>
           <li>Have us delete any data we have about you.</li>
           <li>Express any concern you have about our use of your data</li>
         </ul>

         <h2 class="flow-text center-align">Security</h2>

         <p class="flow-text left-align light">
         We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.

          Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.

          While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.
           </p>

           <h4 class="flow-text left-align">
           If you feel that we are not abiding by this privacy policy, you should contact us immediately via facebook at <a href="#">facebook@jinyuweb</a> or via <a href="mailto:ub17@protonmail.com">email</a>.</h4>

    </div>
 </div>
</div>

<div id='gdpr-anchor'>
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">GDPR</h1>
      
    </div>
</div>

<div class="container z-depth-2">
  <div class="row center">
    <div class="col s12">
      <h2 class="flow-text center-align">Data Policy Contact Permission</h2>

      <p class="flow-text left-align light">We'd like to send you exclusive offers and the latest info from JinyuWeb by email, post and other electronic means.</p>

      <p class="flow-text left-align light">
      	We'll always treat your data like email address with the greates care and will never sell them to other companies for marketing purposes.
      </p>
</div>
</div>
</div>
</div>

<div id='gdpr-anchor'>
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Disclaimer</h1>
    </div>
</div>

<div class="container z-depth-2">
  <div class="row center">
    <div class="col s12">
      <h2 class="flow-text center-align">We are not liable for any consequences of usage other than specified in this disclaimer.</h2>

<p class="left-align">Disclaimer for JinyuWeb</p>

<p class="left-align">
We are doing our best to prepare the service of this site. However, for JinyuWeb cannot warranty the expressions and suggestions of the services, as well as its accuracy. In addition, to the extent permitted by the law, for JinyuWeb shall not be responsible for any losses and/or damages due to the usage of the service on our website, which is just a creation of a user's desired website.

The links contained on our website may lead to external sites, which are provided for convenience only. Any information or statements that appeared in these sites are not sponsored, endorsed, or otherwise approved by our Company. For these external sites, our Company cannot be held liable for the availability of, or the content located on or through it. Plus, any losses or damages occurred from using these contents or the internet generally.</p>

</div>
</div>
</div>
</div>

<div class="container jinyu-card">
<div class="row center">
 <div class="col s12">
     <h2><a href='mailto:ub17@protonmail.com'>Email</a> us for other concerns</h2>
    </div>
    </div>
</div>

% include('footer.tpl')
% include('utils.tpl')