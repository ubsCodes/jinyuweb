% include('navi.tpl')

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Sitemap</h1>
      <div class="row center">
        <h5 class="header col s12 light">Click a link and transfer</h5>
      </div>
    </div>
</div>

<div class="container z-depth-2 jinyu-card">
  <div class="row">
    <div class="col s12">
      
      	<h4><a href="/">Home</a></h4>
      		<ul class='browser-default'>
      			<li><a href="/new/bootstrap">Service</a></li>
      			<li><a href="/inquire">Support</a></li>
      			<li><a href="/jinyus_team">About Us</a></li>
      			<li><a href="/donate">Donate</a></li>
            <li><a href="/tos">Terms of Service</a></li>
      			<li><a href="/privacy_policy">Privacy Policy</a>
      				<ul class='browser-default'>
      					<li><a href="/privacy_policy#gdpr-anchor">GDPR</a></li>
      					
      				</ul>
      			</li>
      			<li><a href="#">Social Media</a>
      				<ul class='browser-default'>
      					<li><a href="https://www.facebook.com/jinyu.webb.7" target="_blank">facebook <img class='social-media' src="images/facebook.png"></a></li>
      					<li><a href="https://www.youtube.com/channel/UCSeeBBdylnccjN12pVgBuiw?view_as=subscriber" target="_blank">youtube <img class='social-media' src="images/youtube.png"></a></li>
      					<li><a href="https://www.instagram.com/jinyuw3b/" target="_blank">instagram <img class='social-media' src="images/instagram.png"></a></li>
      				</ul>
      			</li>
      		</ul>

    
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col s12">
     	 <div class="row center">
        <h5 class="header col s12 light">Be efficient, build your site not in days but in seconds!</h5>
      </div>
      <div class="row center">
        <a href="/new/bootstrap" id="download-button" class="btn-large waves-effect waves-light red">Start Now</a>
      </div>
    </div>
  </div>
</div>



% include('utils.tpl')
