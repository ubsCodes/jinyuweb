<!DOCTYPE html>
  <html>
    <head>
    <meta charset="utf-8">
		<title>JinyuWebb: auto-site builder</title>
		% include('linkcss.tpl')
		<!--% include('linkless.tpl')-->
          <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="google-site-verification" content="Iyv0jLJPv7yaVM2HOdPAQssrszSaNenb1Yq9ZM-FumY" />
    </head>
<body>

<div class="navbar-fixed">
 <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="/" class="brand-logo"><img src=""><span class='logo_name hide-on-small-and-down'>JinyuWeb</span></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/new/materialize">Services</a></li>
        <li><a href="/inquire">Support</a></li>
        <li><a href="/jinyus_team">About Us</a></li>
         <li><a href="/donate">Donate</a></li>
      </ul>

        <ul id="nav-mobile" class="sidenav">
         <li><a href="/new/materialize">Services</a></li>
          <li><a href="/inquire">Support</a></li>
          <li><a href="/jinyus_team">About Us</a></li>
           <li><a href="/donate">Donate</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  </div>

<div class="container">
 <p class='z-depth-2 jinyu-card'>Page not found! </p>
 
    <p>Your IP address is: {{ip}}</p>
    <p>Your browser is: {{env}}</p>
</div>
% include('footer.tpl')
% include('utils.tpl')

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
  });

  // Initialize collapsible (uncomment the lines below if you use the dropdown variation)
  // var collapsibleElem = document.querySelector('.collapsible');
  // var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

  // Or with jQuery

  $(document).ready(function(){
    $('.sidenav').sidenav();
  });
     
</script>