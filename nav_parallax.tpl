<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>JinyuWebb: auto-site builder</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/parallax.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <meta name="google-site-verification" content="Iyv0jLJPv7yaVM2HOdPAQssrszSaNenb1Yq9ZM-FumY" />
</head>
<body>

 <ul id="nav-mobile" class="sidenav">
         <li><a href="/new/materialize">Services</a></li>
          <li><a href="/inquire">Support</a></li>
          <li><a href="/jinyus_team">About Us</a></li>
           <li><a href="/donate">Donate</a></li>
      </ul>

<div class="navbar-fixed">
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="/" class="brand-logo"><img src="images/jinyu_logo5.png"><span class='logo_name hide-on-small-and-down'>JinyuWeb</span></a>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger">
      <img src="images/menubig.png">
      <!--<i class="material-icons">menu</i>--></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/new/materialize">Services</a></li>
        <li><a href="/inquire">Support</a></li>
        <li><a href="/jinyus_team">About Us</a></li>
         <li><a href="/donate">Donate</a></li>
      </ul>

       
   
    </div>
  </nav>
  </div>

    