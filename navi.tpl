<!DOCTYPE html>
  <html>
    <head>
    <meta charset="utf-8">
		<title>JinyuWebb: auto-site builder</title>
		% include('linkcss.tpl')
		
          <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="google-site-verification" content="Iyv0jLJPv7yaVM2HOdPAQssrszSaNenb1Yq9ZM-FumY" />
    </head>
<body>


 

 <ul id="nav-mobile" class="sidenav">
         <li><a href="/new?theme=bootstrap">Services</a></li>
          <li><a href="/inquire">Support</a></li>
          <li><a href="/jinyus_team">About Us</a></li>
           <li><a href="/donate">Donate</a></li>
      </ul>
    
<div class="navbar-fixed">
 <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="/" class="brand-logo"><img src="images/jinyu_logo5.png"><span class='logo_name hide-on-small-and-down'>JinyuWeb</span></a>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><img src="images/menubig.png"></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/new?theme=bootstrap">Services</a></li>
        <li><a href="/inquire">Support</a></li>
        <li><a href="/jinyus_team">About Us</a></li>
         <li><a href="/donate">Donate</a></li>
      </ul>

       
    </div>
  </nav>
  </div>

 

