%#template for editing a task
%#the template expects to receive a value for "no" as well a "old", the text of the selected item


% include('linkcss.tpl')
<!--% include('linkless.tpl')-->
% include('navi.tpl')

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Comments</h1>
      <div class="row center">
        <h5 class="header col s12 light">discussion about {{old[0]}} website</h5>
      </div>
    </div>
</div>

<!--<p>website no. {{no}}</p>-->

<div class='container'>
<form action="/comments/{{no}}" method="get">
<div class='input-field row'>
<div class='col s12 l4'>
<label for='textarea1'>
Comments</label>
</div>

<div class='col s12 l8'> 
<textarea name="comment_list" id="textarea1" class="materialize-textarea" readonly>{{old[4]}} - posted: {{old[5]}}</textarea>
</div>
</div>

<div class='row'>
<div class='col s12 l4'>
<label for='add_comment'>
Add Comments</label>
</div>

<div class='input-field col s12 l8'>
<i class="material-icons prefix">mode_edit</i>
<input type="text" name="comment" size="100" maxlength="100" required>
</div>
</div>

<div class='row'>
<div class='col s12 l4'>
</div>
<div class='input-field col s12 l8'>
  <button class="btn waves-effect waves-light btn-large" name="save" value="save">Save
   &nbsp;
    <img class='send-img' src="images/sendsmall.png">
  </button>
  
</div>
</div>

</form>
</div>

% include('utils.tpl')

