<footer class="page-footer blue" >
    <div class="container" id='footerBG'>
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Company Bio</h5>
          <p class="grey-text text-lighten-4">We are a team of computing enthusiasts working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated. Thank you
          <span>
            % include('donate_btn.tpl')  
          </span>
        </p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="/sitemap">Sitemap</a></li>
            <li><a class="white-text" href="/new">Service</a></li>
            <li><a class="white-text" href="/inquire">Support</a></li>
            <li><a class="white-text" href="/privacy_policy">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="https://www.facebook.com/jinyu.webb.7" target="_blank">Facebook <img class='social-media'  src="images/facebook.png"></a></li>
            <li><a class="white-text" href="https://www.instagram.com/jinyuw3b/" target="_blank">Instagram <img class='social-media'  src="images/instagram.png"></a></li>
            <li><a class="white-text" href="https://www.youtube.com/channel/UCSeeBBdylnccjN12pVgBuiw?view_as=subscriber" target="_blank">Youtube <img  class='social-media'  src="images/youtube.png"></a></li>
            
          </ul>
        </div>
      </div>
    </div>
    
      <div class="container" id='footerBG-copyright'>
			  Copyright &copy; <a class="blue-text text-lighten-3" href="/">jinyuWeb.com</a> &#8226; all rights reserved &#8226; 2018
      </div>
    
  </footer>


    