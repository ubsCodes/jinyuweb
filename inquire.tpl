% include('navi.tpl')


<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">JinyuWeb Support</h1>
      <div class="row center">
        <h5 class="header col s12 light">Your feedback, further web development and more</h5>
      </div>
    </div>
</div>

<div class='container'>
<form action="/inquire" method="GET">

<div class='input-field row'>
<div class='col s12 l4'>
<label for='fname'>Name</label>
</div>

<div class='col s12 l8'> 
<input type="text" name="inquirer" size="100" maxlength="100" required>
</div> 
</div>

<div class='input-field row'>
<div class='col s12 l4'>
<label for='email'>Email</label>
</div>

<div class='col s12 l8'> 
<input type="text" name="inquirer_email" size="100" maxlength="100" required>
</div>
</div>

<div class='input-field row'>
<div class='col s12 l4'>
<label for='textarea2'>Inquiry</label>
</div>

<div class='col s12 l8'> 
<textarea name="inquiry" id="textarea2" class="materialize-textarea">your message here</textarea>
</div>
</div>


<div class='input-field row'>
<div class='col s12 l4'>
{{results}}
</div>
</div>

<div class='input-field row'>
  <button class="btn waves-effect waves-light btn-large" name="send" value="send">Send
    <!--<i class="material-icons right">send</i>--> &nbsp;
    <img class='send-img' src="images/sendsmall.png">
  </button>
</div>

</form>
</div>


<!--% include('footer.tpl') -->
% include('utils.tpl')             
