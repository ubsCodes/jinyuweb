# inicio.py or index/home
# -*- coding: utf-8 -*-
import sqlite3
import datetime
import bottle
from bottle import route, run, template, redirect, request, debug, static_file, error, auth_basic
from datetime import datetime

import login
from login import check_login
import inquire
from inquire import inquire, getDateTimeNow

import jinyus_team
from jinyus_team import jinyus


from buildbots.builder1 import build_files, build_site, apply_theme, apply_sticky_footer, builder, builder_2, zipdir, build_results

import zipfile
import os

#HOST = '127.0.0.1'
#PORT = 8084

HOST = '0.0.0.0'
PORT = 5000

# static files, css, etc.
@route('/css/<filename:path>')
def server_static(filename):
	return static_file(filename, root='css/')

@route('/buildbots/dist/<filename:path>')
def yoursite_static(filename):
    return static_file(filename, root='buildbots/dist/')

@route('/images/<filename:path>')
def server_static(filename):
    return static_file(filename, root='images/')

@route('/new/images/<filename:path>')
def server_static(filename):
    return static_file(filename, root='images/')

@route('/results/jinyu/images/<filename:path>')
def server_static(filename):
    return static_file(filename, root='images/')

@route('/comments/images/<filename:path>')
def server_static(filename):
    return static_file(filename, root='images/')


# static files, js etc.
@route('/js/<filename:path>')
def server_static(filename):
	return static_file(filename, root='js/')

# static files, less etc.
@route('/less/<filename:path>')
def server_static(filename):
	return static_file(filename, root='less/')

# static files sitemap
@route('/<filename:path>')
def site_map(filename):
        return static_file(filename, root='./')

@error(404)
def fourofour(error):
    ipaddr = request.remote_addr 
    envir = request.environ['HTTP_USER_AGENT'] 
    return template('notfound', ip=ipaddr, env=envir)
    
# admin edit
@route('/admin_edit/:no', method='GET')
@auth_basic(check_login)
def edit_item(no):
    """
    Edit a item
    """

    if request.GET.get('save','').strip():
        orders = request.GET.get('orders','').strip()
        amount = request.GET.get('amount','').strip()
        remarks = request.GET.get('remarks','').strip()
        email = request.GET.get('email','').strip()
        date = request.GET.get('date','').strip()

        comment_list = request.GET.get('comment_list','').strip() 
        comment = request.GET.get('add_comment','').strip()
        datetemp = datetime.now()
        date_time = datetemp.strftime('%m/%d/%Y') 
                

        # connect to db and update
        conn = sqlite3.connect('genesis.db')
        c = conn.cursor()
        c.execute("UPDATE orders SET orders = ?, amount = ?, remarks = ?, email = ?, date = ? WHERE order_id LIKE ?", (orders, amount, remarks, email, date, no))
        # if no add comment then only update comment
        if not comment and comment == "": 
            # empty use comment.isspace() or .strip()
            c.execute("UPDATE comments SET comments = ? || ' edited: ' || ?  WHERE comment_id LIKE ?", (comment_list, date_time, no))
 	
        else:       
               
            c.execute("UPDATE comments SET comments = ? || ' \n ' || ? || ' edited: ' || ?  WHERE comment_id LIKE ?", (comment_list, comment, date_time, no))
 

        conn.commit()

        redirect("/admin")
    else:
        conn = sqlite3.connect('genesis.db')
        c = conn.cursor()
        
        c.execute("SELECT o.orders, o.amount, o.remarks, o.email, o.date, c.comments, c.date FROM orders as o INNER JOIN comments as c ON o.comment_id = c.comment_id where order_id LIKE ?", [str(no)])
 

        cur_data = c.fetchone()
        return template('admin_edit', old=cur_data, no=no)



# new comment
@route('/comments/:no', method='GET')
def edit_item(no):
    """
    new comment to add
    """

    if request.GET.get('save','').strip():
        
        comment_list = request.GET.get('comment_list','').strip() 
        comment = request.GET.get('comment','').strip()
        datetemp = datetime.now()
        date_time = datetemp.strftime('%m/%d/%Y') 

        # connect to db and update
        conn = sqlite3.connect('genesis.db')
        c = conn.cursor()
        
        c.execute("INSERT OR IGNORE INTO comments (comment_id, comments, date) VALUES (?,?,?)", (no,comment,date_time))
        # new_id = c.lastrowid

        c.execute("UPDATE comments SET comments = ? || ' \n ' || ? || ' posted: ' || ?  WHERE comment_id LIKE ?", (comment_list, comment, date_time, no))
 
        conn.commit()

        redirect("/comments/" + no)
    else:
        conn = sqlite3.connect('genesis.db')
        c = conn.cursor()
        c.execute("SELECT o.orders, o.amount, o.remarks, o.date, c.comments, c.date FROM orders as o INNER JOIN comments as c ON o.comment_id = c.comment_id where order_id LIKE ?", [str(no)])
        
        cur_data = c.fetchone()
        return template('comments', old=cur_data, no=no)




@route('/admin')
@auth_basic(check_login)
def bucket_list():
    conn = sqlite3.connect('genesis.db')
    c = conn.cursor()
    c.execute("SELECT order_id, orders, amount, remarks, email, date FROM orders;")
    result = c.fetchall()
    c.close()
    output = template('wish_table', rows=result)
    return output



# home
@route('/')
def bucket_list():
    conn = sqlite3.connect('genesis.db')
    c = conn.cursor()
    c.execute("SELECT order_id, orders, amount, remarks, date FROM orders ORDER BY order_id DESC LIMIT 8;")
    result = c.fetchall()
    c.close()
    output = template('wish_table_home', rows=result)
    return output

# busqueda == search in english
@route('/<busqueda>')
@auth_basic(check_login)
def show_item(busqueda):

        conn = sqlite3.connect('genesis.db')
        c = conn.cursor()
        busqueda=busqueda+"%"
        c.execute("SELECT order_id, orders, amount, remarks, date FROM orders WHERE orders LIKE ?", [busqueda])
        result = c.fetchall()
        c.close()

        if not result:
            return "That page does not exist! <a href='/'>back</a>"
        else:
           return template('wish_table', rows=result)



@route("/new", method="GET")
def new_item():
    """
    Add a new item
    """
    if request.GET.get('save','').strip():
        orders = request.GET.get('order','').strip()
        amount = request.GET.get('amount','').strip()
        remarks = request.GET.get('remarks','').strip()
        email = request.GET.get('email','').strip() 
        datetemp = datetime.now()
        date_time = datetemp.strftime('%m/%d/%Y')        
        comment = "..."      
        tos_check_agreed = ''
        zipped_type = ''
        tos_checked = request.GET.get('tosGdprAgree','').strip()
        
        menu = request.GET.get('site_menu','').strip()
        theme = request.GET.get('site_theme','').strip()

        if tos_checked:
            tos_check_agreed = 'yes'
        else:
            tos_check_agreed = 'no'

        conn = sqlite3.connect('genesis.db')
		
        c = conn.cursor()
		
	   # get order id, last     
        c.execute("SELECT order_id FROM orders ORDER by order_id desc LIMIT 1;")
        last_id = c.fetchone()[0]
        last_id = int(last_id)
        last_id += 1
        
        c.execute("INSERT INTO orders (orders,amount,remarks,email,date,comment_id) VALUES (?,?,?,?,?,?)", (orders,amount,remarks,email,date_time,last_id) )
        # new_id = c.lastrowid
		
	   # insert blank comments
        c.execute("INSERT INTO comments (comment_id, comments, date) VALUES (?,?,?)",(last_id,comment,date_time) )
        # new_id = c.lastrowid

        #insert site details
        c.execute("INSERT INTO site_details (order_id, site_menu, site_theme) VALUES (?,?,?)", (last_id, menu, theme) )

        #insert site tos data
        c.execute("INSERT INTO tos_data(order_id, tos_agreed) VALUES (?,?)", (last_id, tos_check_agreed) )
 
 
        conn.commit()
        c.close()
        
        sitetitle = orders + ' - ' + remarks 

        # build it
        if theme == 'bootstrap' or theme == 'materialize' or theme == 'windowsflat':
            builder_2(sitetitle, menu, orders, remarks, theme)
            zipit('specialsite.zip', './buildbots/dist')
            zipped_type = 'specialsite.zip'

        else:
            builder(sitetitle, menu, orders, remarks, theme)
            zipit('yourwebsite.zip', './buildbots/dist')
            zipped_type = 'yourwebsite.zip'
        
        
        redirect("/results/" + orders + "/" + zipped_type)
    else:
        param = request.query.get('theme','')
        return template("add_order", theme_panel=param)


def zipit(filename, _path):
    zipf = zipfile.ZipFile(filename,'w', zipfile.ZIP_DEFLATED)
    zipdir(_path, zipf)
    zipf.close()


@route('/del/:no', method='GET')
@auth_basic(check_login)
def del_item(no):
    """
    delete a item
    """
    conn = sqlite3.connect('genesis.db')
    c = conn.cursor()
    c.execute("DELETE FROM orders WHERE order_id LIKE ?", [str(no)])
    conn.commit()
    c.close()

    redirect("/admin")


@route('/sitemap')
def site_map():
    return template("sitemap.tpl")

@route('/privacy_policy')
def privacy():
    return template("privacy_policy.tpl")

@route('/tos')
def tos():
    return template('tos.tpl')
    


#run(host=HOST, port=PORT, reload=False, debug=False)

if os.environ.get('APP_LOCATION') == 'heroku':
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
else:
    run(host='localhost', port=8084, debug=True)
