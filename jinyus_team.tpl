% include('nav_parallax.tpl')

 <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2">About Us</h1>
        <div class="row center">
          <h5 class="header col s12 light">The founders of JinyuWeb and their stories</h5>
        </div>
       
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="images/index.jpeg" alt="Unsplashed background img 1"></div>
  </div>

   
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">

          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
	          <span class="card-title">Roka Sanjita</span><p>ろ過</p>
	        </div>
	        <div class="card-content">
	          <h5>Web Designer</h5> <p>
			      Roka, is an avid fan of town lottery, a great chess and badminton player. He wants to be a ,an astronaut someday. He programs in obscure applications in c and java programming languages. He is one of our talented web designers.</p>
	        </div>
	        <div class="card-action">
	          <a href="mailto:rokasanjita@tuta.io">E-mail me</a>
	        </div>
	      </div>
          
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">

       <div class="card blue-grey darken-1">
            <div class="card-content white-text">
	          <span class="card-title">Danno Akira</span><p>アキラ</p>
	        </div>
	        <div class="card-content">
	          <h5>Program Manager</h5> <p> Kira, as we fondly call him,  believes
				  we're gonna rock the unknown seas of the web, makin' it big like
				  facebook. He likes foreign culture like russia, likes hollywood celebs and goin' to the midnight clubs, he codes in cellphones in using javascript and ruby.</p>
	        </div>
	        <div class="card-action">
	         <a href="mailto:dannokira@yopmail.com">E-mail me</a>
	        </div>
	      </div>
           
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
           
           <div class="card blue-grey darken-1">
            <div class="card-content white-text">
	          <span class="card-title">Toshiru Mitsu</span><p>みつ</p>
	        </div>
	        <div class="card-content">
	          <h5>Lead Programmer</h5> <p> Likes python and game development. Likes to program anywhere, in commuting, in the restroom,
					at public parks, at the mall, etc. He likes to learn new stuff but masters just a few
          . Likes anime, videogames, weird stuff, and ancient history too.</p>
	        </div>
	        <div class="card-action">
	         <a href="mailto:ub17@protonmail.com">E-mail me</a>
	        </div>
	      </div>
          
          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">A futuristic way to build magical, beautiful and efficient websites.</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/background2.jpg" alt="Unsplashed background img 2"></div>
  </div>

  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Contact Us</h4>
          <p class="left-align light">Email:  <a href="mailto:ub17@protonmail.com" _target='blank'>jinyuWeb@protonmail</a>
          <br>facebook: <a href="https://facebook.com/jinyuWeb" _target='blank'>https://facebook.com/jinyuWeb</a> <br>Twitter: 
          <a href="https://www.youtube.com/channel/UCSeeBBdylnccjN12pVgBuiw?view_as=subscriber" _target='blank'>jinyuweb@youtube</a></p>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">In just a few seconds, your website will be created, lightning fast!</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/background3.jpg" alt="Unsplashed background img 3"></div>
  </div>


% include('footer.tpl')
% include('utils.tpl')
<script type="text/javascript">
	(function($){
	  $(function(){

	    $('.sidenav').sidenav();
	    $('.parallax').parallax();

	  }); // end of document ready
	})(jQuery); // end of jQuery name space
</script>
