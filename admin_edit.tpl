%#template for editing a task as admin
%#the template expects to receive a value for "no" as well a "old", the text of the selected item

% include('linkcss.tpl')
% include('linkless.tpl')
% include('navi.tpl')


<p>Edit Comments for Order No. {{no}}</p>

<div class='container'>
<form action="/admin_edit/{{no}}" method="get">


<div class='row'>
<div class='col-25'>
<label for='order'>
Order</label>
</div>

<div class='col-75'>
<input type="text" name="orders" value="{{old[0]}}" size="100" maxlength="100">
</div>
</div>

<div class='row'>
<div class='col-25'>
<label for='pledge'>
Pledge</label>
</div>

<div class='col-75'>
<input type="text" name="amount" value="{{old[1]}}" size="100" maxlength="100">
</div>
</div>

<div class='row'>
<div class='col-25'>
<label for='remarks'>
Remarks</label>
</div>

<div class='col-75'>
<input type="text" name="remarks" value="{{old[2]}}" size="100" maxlength="100">
</div>
</div>

<div class='row'>
<div class='col-25'>
<label for='email'>
Email</label>
</div>

<div class='col-75'>
<input type="text" name="email" value="{{old[3]}}" size="100" maxlength="100">
</div>
</div>

<div class='row'>
<div class='col-25'>
<label for='date'>
Date Posted</label>
</div>

<div class='col-75'>
<input type="text" name="date" value="{{old[4]}}" size="100" maxlength="100">
</div>
</div>

<div class='row'>
<div class='col-25'>
<label for='comments'>
Comments</label>
</div>

<div class='col-75'>
<textarea name="comment_list" rows="8" cols="100">{{old[5]}} - {{old[6]}}
</textarea>
</div>
</div>


<div class='row'>
<div class='col-25'>
<label for='add_comment'>
Add Comments</label>
</div>

<div class='col-75'>
<input type="text" name="comment" size="100" maxlength="100" required>
</div>
</div>

<div class='row'>
<input type="submit" name="save" value="Save">
</div>

</form>
</div>

