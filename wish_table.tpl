<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>JinyuWebb: cheap site builders</title>
% include('linkcss.tpl')
% include('linkless.tpl')
</head>

<body>
% include('navi.tpl')

%#template to generate a HTML table from a list of tuples
<h2>Site requests at JinyuWebb!</h2> 

<p>Admin - edit, delete orders</p>

<table border="1">
<tr><th>#</th><th>Orders</th><th>Pledge</th>
<th>Remarks</th><th>Email</th><th>Date</th><th>Actions</th></tr>

%for row in rows:
%id, ord, amt, rem, em, datetime = row

<tr>
%for col in row:
<td>{{col}}</td>
%end
<td><a href="/admin_edit/{{id}}"> Edit</a></td>
<td><a href="/del/{{id}}"> Del</a></td>
</tr>
%end

</table>
<p>Create <a href="/new">New</a> order</p>

<br>
<br>

% include('footer.tpl')
% include('utils.tpl')
</body>
</html>

