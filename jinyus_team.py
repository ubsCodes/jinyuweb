# the about page, jinyus team

import sqlite3
import datetime
import bottle
from bottle import route, run, template, redirect, request, debug, static_file, error, auth_basic
from datetime import datetime

@route('/jinyus_team')
def jinyus():
	return template('jinyus_team')

@route('/donate')
def donate():
	return template('donate')
