%#template to generate a HTML table from a list of tuples
% include('navi.tpl')

 <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Build your site in seconds</h1>
      <div class="row center">
        <h5 class="header col s12 light">Using our advance AI technology, automagically</h5>
      </div>
      <div class="row center">
        <a href="/new" id="download-button" class="btn-large waves-effect waves-light red">Get Started</a>
      </div>
      <br><br>

    </div>
  </div>

<!-- choose a theme -->
<div class="container z-depth-2">
  <div class="row center">
    <div class="col s12">
      <p class="flow-text light"> Choose a theme</p>
    </div>
  </div>
  
  <div class="row">

    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/90s.png">
          <span class="card-title"><div class="card-panel teal lighten-2">90s</div></span>
          <a href="/new?theme=90s" class="btn-floating halfway-fab waves-effect waves-light red">
          <img src="images/addbig.png">
          <!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>A blast from the past indeed. The 90s theme features simple and functional design that loads fast. We added some modern effects too, like mobile responsiveness.</p>
        </div>
      </div>
    </div>

    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/hipncool.png">
          <span class="card-title"><div class="card-panel teal lighten-2">Hip n Cool</div></span>
          <a href="/new?theme=hipncool" class="btn-floating halfway-fab waves-effect waves-light red"> <img src="images/addbig.png"><!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>Modern fab look. Nice and fresh pastel colors abound with big lovely fonts. Typography combination of helvetica and lobster is just awesome!</p>
        </div>
      </div>
    </div>    
  </div>

  <div class="row">
    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/simple.png">
          <span class="card-title"><div class="card-panel teal lighten-2">Simple</div></span>
          <a href="/new?theme=simple" class="btn-floating halfway-fab waves-effect waves-light red"> <img src="images/addbig.png"></a><!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>Simple hue of blue exudes professional feel like those big social media sites. Get this one if you like
          a basic and convenient site. Very light, simple yet pretty.</p>
        </div>
      </div>

    </div>


    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/bootstrap.png">
          <span class="card-title"><div class="card-panel teal lighten-2">Bootstrap</div></span>
          <a href="/new?theme=bootstrap" class="btn-floating halfway-fab waves-effect waves-light red"> <img src="images/addbig.png"></a><!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>From twitter company itself. Very professional and legit theme. Used by some big corporation like sony, coca cola and more. You can never go wrong with this.</p>
        </div>
      </div>
    </div>
  </div>


    <div class="row">

    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/materialize.png">
          <span class="card-title"><div class="card-panel teal lighten-2">Materialize</div></span>
          <a href="/new?theme=materialize"' class="btn-floating halfway-fab waves-effect waves-light red"> <img src="images/addbig.png"></a><!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>Derived from material theme, google's own prolific design. A representation of the real physical world. This design is based on science and physics, truly sublime.</p>
        </div>
      </div>
    </div>

    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-image">
          <img src="images/windowsflat.png">
          <span class="card-title"><div href='/new/windowsflat' class="card-panel teal lighten-2">Windows Flat</div></span>
          <a href='/new?theme=windowsflat' class="btn-floating halfway-fab waves-effect waves-light red"> <img src="images/addbig.png"><!--i class="material-icons">add</i--></a>
        </div>
        <div class="card-content">
          <p>Coming from microsoft's fabric theme. This one is very windows-like, clean flat edges. A modern contemporary design, future proof and responsive.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
</br>
<div class="container">
  <div class="row center">
    <div class="col s12 m12 l12">
      <p class="flow-text light"> How to buid your site in seconds? </p>
    </div>
    <div class="col s12 m12 l12">
      <p class="flow-text light"> follow our 3 easy steps</p>
    </div>
  </div>
  <div class="row center">
    <div class="col s12">
      <p class="flow-text light">1. Click on 'get started' or 'try now!' below <br>2. Fill up form for site info e.g. site name, theme <br>3. Click on 'Build' </p>
    </div>
  </div>
  <div class="row center">
    <div class="col s12">
      <a href="/new" class="btn-large waves-effect waves-light orange">Try now!</a>
    </div>
  </div>  
</div>

<br><br>
<!--  table of newly created sites -->
<div class="container z-depth-2">
<h2 class='light center'>Recently created sites</h2> 
<table class='responsive-table striped centered'>
<tr><th>#</th><th>Sitename</th><th>Pledge</th>
<th>Tagline</th><th>Date</th><th>Action</th></tr>


%for row in rows:
%id, ord, amt, rem, date = row

<tr>
%for col in row:
<td>{{col}}</td>
%end
<td><a href="/comments/{{id}}"> Comments</a></td>

</tr>
%end

</table>
<div class="row center">
 <a href="/new/materialize" id="download-button" class="btn-large waves-effect waves-light yellow grey-text text-darken-3">Build a site</a>
</div>

</div>

<br>
<br>



<div class="container">
  <div class="row">
    <div class="col s12">
      <p class="flow-text z-depth-2 jinyu-card">新しい注文をクリック <a href="/new">ここに</a> あなたのサイトを構築し、何らかの金額を支払うか、寄付します ;)</p>
    </div>
  </div>
</div>

% include('footer.tpl')
% include('utils.tpl')

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
  });

  // Initialize collapsible (uncomment the lines below if you use the dropdown variation)
  // var collapsibleElem = document.querySelector('.collapsible');
  // var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

  // Or with jQuery

  $(document).ready(function(){
    $('.sidenav').sidenav();
  });
     
</script>

