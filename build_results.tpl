% include('navi.tpl')

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Build Results</h1>
      <div class="row center">
        <h5 class="header col s12 light">
          
          % if dlist is None:
            <h3>failed...</h3>
          % else: 
            <h3>success!</h3>
          % end

        </h5>
      </div>
    </div>
</div>

<!--
<h2>Build Results!</h2>
% if dlist is None:
  <h3>failed</h3>
% else: 
  <h3>success</h3>
% end
-->

<div class="container">
<div class="z-depth-2 jinyu-card">

<div class="blue-text">Files created:</div>
<ul>
% for list in dlist:
    % if list == 'ursite.html':
        <li>{{site}}.html <a href='/preview/ursite.html' target='_blank'>  Preview {{site}}! </a></li>
    % elif list == '__init__.py':
        <li>init.py</li>
    % else:
        <li>{{list}}</li>
    %end
% end
</ul>

<div> <span class="blue-text">website: </span>{{site}} </div> 


</div>
</div>




<div class="intrinsic-container intrinsic-container-16x9">
<iframe src="/preview/ursite.html" allowfullscreen>
  <p>Your browser does not support iframes.</p>
</iframe>
</div>

<br><br>

<div class='container'>
<div class="z-depth-2 jinyu-card">
<div class='row center'>
<div class="col s12 m12 l12">
  By clicking the 'download' button below, you are agreeing to our <a href="/tos">Terms Of Service</a> &amp; <a href="/privacy_policy#gdpr-anchor">GDPR</a>
  </div>


<div class="col s12 m12 l12"><a href='/site/{{zipfile_type}}'><h2>Download</h2></a></div>

<div class="col s12 m12 l12">
% include('donate_btn.tpl')
</div>

</div>

  </div>
</div>
<br><br>




% include('footer.tpl')

% include('utils.tpl')
