% include('navi.tpl')

 <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Create a Website</h1>
      <div class="row center">
        <h5 class="header col s12 light">Just fill this up and submit</h5>
      </div>
    </div>
</div>


<div class='container'>
<form action="/new" method="GET">

<div class='row'>
<div class='col s4'>
<label for='Order'>Site Name</label>
</div>

<div class='col s8'>
<div class="input-field">
<input type="text" name="order" size="100" maxlength="100" placeholder='e.g. vasebook, teether, instagame' required>
</div>
</div>
</div>

<div class='row'>
<div class='col s4'>
<label for='Amount'>Pledge or Donation</label>
</div>

<div class='col s8'>
<div class="input-field">
<input type="text" name="amount" size="100" maxlength="100" placeholder='e.g. $5' required>
</div>
</div>
</div>

<div class='row'>
<div class='col s4'>
<label for='Remarks'>Site Tagline</label>
</div>

<div class='col s8'>
<div class="input-field">
<input type="text" name="remarks" size="200" maxlength="500" placeholder='e.g. just did it, we find wayz, etc.'>
</div>
</div>
</div>

<div class='row'>
<div class='col s4'>
<label for='Email'>Email</label>
</div>

<div class='col s8'>
<div class="input-field">
<input type="text" class="validate" name="email" size="100" maxlength="100" placeholder='e.g. jinyusgang@yahoo.com' required>
</div>
</div>
</div>

<div class='row'>
<div class='col s4'>
<label for='site_menu'>Site menu</label>
</div>

<div class='col s8'>
<div class="input-field">
<input type="text" name="site_menu" size="100" maxlength="100" placeholder='e.g. home, about us, blog, services, contact us'>
</div>
</div>
</div>

<div class='row'>
<div class='col s4'>
<label for='site_theme'>Site theme</label>
</div>

<div class='col s8'>
<div class="input-field col s8">
<select name='site_theme' class='select-theme'>
<option value='simple'>Simple</option>
<option value='hipncool'>HipnColorful</option>
<option value='90s'>90s</option>
<option value='bootstrap'>bootstrap</option>
<option value='materialize'>materialize</option>
<option value='windowsflat'>windows-flat</option>
</select>
</div>
</div>
</div>

<!--terms and conditions-->
<div class='row'>
<div class='col s4'>
<label for='site_menu'>Agree with <a href="/tos">Terms of Service</a> &amp; <a href="/privacy_policy#gdpr-anchor">GDPR</a></label>
</div>

<div class='col s8'>

<div class="input-field">
 <p>
      <label>
        <input type="checkbox" name='tosGdprAgree' id='tosGdprAgreeYes' class='tosgdpr' />
        <span>I Agree</span>
      </label>
 
    </p>
         </div>

  <div class="input-field">       
    <p>
      <label>
        <input type="checkbox" name='tosGdprDisagree' id='tosGdprAgreeNo' class='tosgdpr' />
        <span>Disagree</span>
      </label>
   
    </p>   </div>




</div>
</div>

<br>
<div class='row center'>
  <button id="submitSite" class="btn waves-effect waves-light btn-large" name="save" value="submit">Submit
  <!--i class="material-icons right">send</i--> &nbsp;
    <img class='send-img' src="images/sendsmall.png">
  </button>
</div>
</form>
</div>

<div class="theme_panel" style="display: none">{{theme_panel}}</div>

% include('footer.tpl')
% include('utils.tpl')


<script type="text/javascript">
var setTheme = (function(){
	$(function(){
		var theme = $('.theme_panel').text();
		//console.log(theme)		
		$('.select-theme').find('option[value=' + theme + ']').prop('selected', true);
		$('.select-theme').formSelect();
	});
})();
</script>
