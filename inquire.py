# for inquiries

import sqlite3
import datetime
import bottle
from bottle import route, run, template, redirect, request, debug, static_file, error, auth_basic
from datetime import datetime



@route('/inquire', method='GET')
def inquire(): 

	 
	if request.GET.get('send','').strip():
		inquirer = request.GET.get('inquirer','').strip()
		inquirer_email = request.GET.get('inquirer_email').strip()
		inquiry = request.GET.get('inquiry').strip()
		date_time = getDateTimeNow()

		# connect to db
		conn = sqlite3.connect('genesis.db')

		c = conn.cursor()
		c.execute("INSERT INTO inquiries (inquirer, inquirer_email, inquiry, date_time) VALUES (?,?,?,?)", (inquirer, inquirer_email, inquiry, date_time))

		conn.commit()

		if c.rowcount == 1:
                    c.close()
                    get_results = 'success! we will reply to you asap'
                    # redirect('/inquire')
                    return template('inquire', results=get_results)
		else:
		    c.close()
		    get_results = 'something went wrong...'
		    return template('inquire', results=get_results)
	else:   
            get_results = ''       
            return template('inquire', results=get_results)

def getDateTimeNow():
	return datetime.now().strftime('%m/%d/%Y')
